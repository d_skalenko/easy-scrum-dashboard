Easy-scrum dashboard for distributed teams

-----------------------
Screenshots:

[Initial login screen screenshot](screenshots/DashboardScreenshot1.png)

[Second login knows your previous account screenshot](screenshots/DashboardScreenshot3.png)

[Main application screen](screenshots/DashboardScreenshot2_80.png)

[Two people in the same room](screenshots/DashboardScreenshot4_80.png)

-----------------------

Features to add (prioritized):

- (DONE) Tweet your status + limit message to 140 characters
- (DONE) Quick login username
- (DONE) Ability to change avatar (save to db and host it)
- (DONE) save to cookies last login and show list of accounts with option to "login with different account"
- (DONE) Animate status feed
- (DONE) !!! test performance of system after generating lots of garbage entities into some team and loading it
- (DONE) Chat history
- (DONE) Options menu with buttons: profile (not sure if needed), join team
- (DONE) shared files with drag&drop
- (DONE) prevent broken input (room, user names and tweets)
- refactoring! (beautify all the code), switch sysouts to logback
- (DONE) improve tweet input (multiline, support links), handle enter and escape keys
- Show last user status on hover
- (DONE) redesign part 1
- redesign to be more appealing: better chat (day delimiters and new design), 100% height, merge user list with chat
- user tabs (private messaging)
- highlighting on new messages
- test design with a lot of users in a room
- team tabs (useful for scrum masters with multiple teams)
- "More" in chat history
- "More" in tweets
- (DONE) try to improve unsafe ng-bind-html usage and prevent malicious content - A: actually it is safe enough
- Allow first guys in a room to be admin and approve other members. Admin's Plus button to approve joined users to the team and assign roles
- if admin hasn't logged in for some time (a week) - let other guy become admin
- Move new tweet input to modal + button to tweet should be above statuses
- Add Team background for header + Move user avatar to the middle <OR> move profile info to left + expand chat and user list
- Admin's Status request for the team members
- make important type of tweet (questions, requests, etc.) pin and stack important tweets on top
- improve db performance (paging, caching, optimize entities, write unit tests)
- replace notifier with noty lib

ADVANCED

- "... is typing" in chat
- optimize akka messaging (do not use java serializer)
- clusterization support (main webapp arbiter which load balances rooms betweet available nodes)
- password protected rooms (premium?)


-----------------------
How to run:

1) gradle build

2) start persistent h2 database server by running db/h2-server.sh or db/h2-server.bat

3) gradle bootRun
/**
 * Created by vx on 8/17/15.
 */
var services = angular.module('ng.services.fileUpload', []);

services.service('fileUpload', ['$http', function ($http) {
  this.uploadFileToUrl = function(file, userName, uploadUrl){
    var fd = new FormData();
    fd.append('file', file);
    fd.append('userName', userName);
    return $http.post(uploadUrl, fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    });
  }
}]);
/**
 * Created by vx on 7/25/2015.
 */
var services = angular.module('ng.services.push', []);

services.factory('pushService', ['atmosphereService', function (atmosphereService) {

  return {
    defaultRequest: function() {
        return {
            contentType: 'application/json',
            logLevel: 'debug',
            transport: 'websocket',
            trackMessageLength: true,
            reconnectInterval: 5000,
            enableXDR: true,
            timeout: 180000
        };
    },
    registerDefaultHandlers: function (model, request, socket, drawMessageFunc, afterConnectFunc) {
      request.onOpen = function (response) {
        model.transport = response.transport;
        model.connected = true;
        model.uuid = response.request.uuid;
        drawMessageFunc('SYSTEM', new Date(), 'Atmosphere connected using ' + response.transport);
        if (afterConnectFunc) {
          afterConnectFunc();
        }
      };

      request.onClientTimeout = function (response) {
        drawMessageFunc('SYSTEM', new Date(), 'Client closed the connection after a timeout. Reconnecting in ' + request.reconnectInterval);
        model.connected = false;
        socket.push(atmosphere.util.stringifyJSON({ sender: model.name, message: 'is inactive and closed the connection. Will reconnect in ' + request.reconnectInterval }));
        setTimeout(function () {
          socket = atmosphereService.subscribe(request);
        }, request.reconnectInterval);
      };

      request.onReopen = function (response) {
        model.connected = true;
        model.uuid = response.request.uuid;
        drawMessageFunc('SYSTEM', new Date(), 'Atmosphere re-connected using ' + response.transport);
        if (afterConnectFunc) {
          afterConnectFunc();
        }
      };

      request.onTransportFailure = function (errorMsg, request) {
        atmosphere.util.info(errorMsg);
        request.fallbackTransport = 'streaming';
        drawMessageFunc('SYSTEM', new Date(), 'Atmosphere Chat. Default transport is WebSocket, fallback is ' + request.fallbackTransport);
      };

      request.onClose = function (response) {
        model.connected = false;
        drawMessageFunc('SYSTEM', new Date(), 'Server closed the connection after a timeout');
        socket.push(atmosphere.util.stringifyJSON({ sender: model.name, message: 'disconnecting' }));
      };

      request.onError = function (response) {
        drawMessageFunc('SYSTEM', new Date(), "Sorry, but there's some problem with your socket or the server is down");
        model.logged = false;
      };

      request.onReconnect = function (request, response) {
        drawMessageFunc('SYSTEM', new Date(), 'Connection lost. Trying to reconnect ' + request.reconnectInterval);
        model.connected = false;
      };
    }
  }
}]);
/**
 * Created by vx on 6/29/2015.
 */
'use strict';

angular.module('ngApp', [
    'ngRoute',
    'ngResource',
    'ng.services.rest',
    'ng.services.atmosphere',
    'ng.services.noty',
    'ng.services.push',
    'ng.services.fileUpload',
    'ng.services.cookie',
    'ng.controllers',
    'ng.controllers.dashboard'
])
    .config(function ($routeProvider, $httpProvider) {
        $routeProvider.when('/login', {templateUrl: 'views/login.html', controller: 'LoginController'});
        $routeProvider.when('/dashboard', {templateUrl: 'views/dashboard.html', controller: 'DashboardController'});
        $routeProvider.otherwise({redirectTo: '/login'});

        /* CORS... */
        /* http://stackoverflow.com/questions/17289195/angularjs-post-data-to-external-rest-api */
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    })

    // making auto-redirect to login if $rootScope.username was not yet specified
    .run( function($rootScope, $location) {
        // register listener to watch route changes
        $rootScope.$on( "$routeChangeStart", function(event, next, current) {
            if ( $rootScope.username == null ) {
                // no logged user, we should be going to #login
                if ( next.templateUrl == "views/login.html" ) {
                    // already going to #login, no redirect needed
                } else {
                    // not going to #login, we should redirect now
                    $location.path( "/login" );
                }
            }
        })
    });
/**
 * Created by vx on 6/29/2015.
 */
var app = angular.module('ng.controllers', []);

app.controller('ParentPageController', ['$scope', 'noty', function ($scope, noty) {

    $scope.noty = noty;

// ================================================================
    // on startup
    noty.info('Loaded');
}]);
//===========================================================================================================================================
//===========================================================================================================================================
//===========================================================================================================================================
//===========================================================================================================================================

app.controller('LoginController', ['$scope', '$rootScope', '$location', '$interval', 'cookieService', function ($scope, $rootScope, $location, $interval, cookieService) {

    $scope.validationPattern = new RegExp("^[a-zA-Z0-9_]*$");
    $rootScope.loggedIn = false;
    $rootScope.prevLoginsCookieName = "previousLogins";
    $rootScope.previousAccounts = []; // {name, team, userPic}
    $scope.showLoginForm = true;
    $scope.isError = false;

    var previousString = cookieService.readCookie($rootScope.prevLoginsCookieName);

    if (previousString !== undefined && previousString !== null) {
        $rootScope.previousAccounts = JSON.parse(previousString);
        $rootScope.previousAccounts.sort(function (a, b) {
            return a.name > b.name;
        })
    }

    if ($rootScope.previousAccounts.length !== 0) {
        $scope.showLoginForm = false;
    }

    $scope.onLogin = function (name, team) {
        if (!isValidInput(name) || !isValidInput(team)) {
            $scope.isError = true;
            return;
        }
        $rootScope.loggedIn = true;
        $rootScope.username = name;
        $rootScope.team = team;

        $location.path("/dashboard");
    }

    function isValidInput(val) {
        return val && $scope.validationPattern.test(val);
    }

    $scope.showLoginFormTrue = function () {
        $scope.showLoginForm = true;
    }

//    stolen from https://www.reddit.com/r/programming/comments/3hpnhv/that_mesmerizing_thing_in_18_lines_of_pure/
    var pos=0;
    var pinc=.25

    $scope.drawit=function(){
        if ($rootScope.loggedIn) {
            $interval.cancel($scope.promise);
            return;
        }
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        ctx.clearRect(0, 0, c.width, c.height);
        for(var x=0;x< 10; x++){
            var r = x*10 +4;
            var st=((pos * (x+1)) % 200)/100;
            var et=(st + 1) % 2;

            ctx.beginPath();
            ctx.lineWidth=8;
            ctx.arc(100, 100, r, et * Math.PI , st * Math.PI);
            ctx.stroke();
            //console.log(r + " " + pos + " " +st + " " + et);
            pos=(pos+pinc)  %200;
        }
        pinc=.2 * ((100-(Math.abs(100-pos)))/100);
        if (pinc<.01) pinc=.01;
    }

    $scope.promise = $interval(function() { $scope.drawit(); } , 16);
}]);


/**
 * Created by vx on 7/3/15.
 */
"use strict";
var app = angular.module('ng.controllers.dashboard', [
    'luegg.directives', // chat sticky scroll
    'ngSanitize' // angular bind-as-html attr to allow links in tweets (ng-bind-html)
]);

app.controller('DashboardController', ['$scope', '$rootScope', 'noty', 'atmosphereService', 'pushService', 'fileUpload', 'cookieService',
    function ($scope, $rootScope, noty, atmosphereService, pushService, fileUpload, cookieService) {

        $scope.room = {
            members: [],
            tweets: [],
            currentUser: undefined,
            onlineMembers: function () {
                return this.members.filter(function (m) {
                    return m.online && m.name != $scope.model.name;
                })
            },
            sortMembers: function () {
                $scope.room.members.sort(function (a, b) {
                    if (a.online && !b.online) {
                        return -1;
                    }
                    if (!a.online && b.online) {
                        return 1;
                    }
                    return a.name.localeCompare(b.name);
                })
            }
        }
        $scope.model = {
            transport: 'websocket',
            messages: [],
            name: $rootScope.username,
            team: $rootScope.team,
            uuid: 0
        };
        $scope.filesharingInvites = {
            pending: [
            ], // {user, sessionId}
            pendingOutgoing: [
            ], // {user, sessionId}
            sessions: [] // {user, sessionId, isAccepted, isConnected, connection}
        }
        var filesharingSignalingUrl = "ws://" + location.hostname + ":8080/ws/filesharing";
        var chatSocket;
        var chatRequest = pushService.defaultRequest();
        chatRequest.url = "/ws/room/" + $rootScope.team + "/user/" + $rootScope.username;

        $scope.chatMessage = function (author, date, message) {
            $scope.model.messages.push({sender: author, date: new Date(date), text: message});
        }
        var pushToChatSocket = function (messageObject) {
            messageObject.direction = 'TO_SERVER';
            chatSocket.push(atmosphere.util.stringifyJSON(messageObject));
        }
        $scope.sendTweet = function (tweet) {
            console.log("sending tweet " + tweet);
            pushToChatSocket({type: 'TWEET', sender: $scope.model.name, team: $scope.model.team, text: tweet, date: new Date()});
        }
        $scope.sendLogInMessage = function () {
            pushToChatSocket({type: 'LOGGED_IN', sender: $scope.model.name, team: $scope.model.team, uuid: $scope.model.uuid});
        };

        pushService.registerDefaultHandlers($scope.model, chatRequest, chatSocket, $scope.chatMessage, $scope.sendLogInMessage)

        chatRequest.onMessage = function (response) {
            var responseText = response.responseBody;
            try {
                var message = atmosphere.util.parseJSON(responseText);
                if (!$scope.model.logged && $scope.model.name)
                    $scope.model.logged = true;

                var date = typeof(message.time) === 'string' ? parseInt(message.time) : message.time;
                switch (message.type) {
                    case 'CHAT':
                        $scope.chatMessage(message.sender, date, message.message);
                        break;
                    case 'CHAT_HISTO':
                        var histo = message.histo;
                        for (var i = 0; i < histo.length; i++) {
                            $scope.chatMessage(histo[i].sender, histo[i].time, histo[i].message);
                        }
                        break;
                    case 'LOGGED_IN':
                        $scope.chatMessage('SERVER', date, message.sender + " has logged in!");
                        break;
                    case 'DISCONNECTED':
                        $scope.getMemberByName(message.sender).online = false;
                        $scope.room.sortMembers();
                        $scope.chatMessage('SERVER', date, message.message);
                        break;
                    case 'TWEET':
                        $scope.room.tweets.unshift(message); //add to the top of tweets
                        $scope.chatMessage('SERVER', date, "new tweet!");
                        break;
                    case 'STATUS':
                        if ($scope.model.team === message.teamName) {
                            $scope.room.members = message.members;
                            $scope.room.sortMembers();
                            $scope.room.members.forEach(function (member) {
                                if (member.userPic === null) {
                                    member.userPic = "/res/img/user.png";
                                }
                            })
                            $scope.room.tweets = message.tweets;

                            if ($scope.room.currentUser === undefined) {
                                $scope.room.currentUser = $scope.getMemberByName($scope.model.name);
                                var loginObj = {
                                    name: $scope.model.name,
                                    team: $scope.model.team,
                                    userPic: $scope.room.currentUser.userPic
                                }
                                for (var i = 0; i < $rootScope.previousAccounts.length; i++) {
                                    if ($rootScope.previousAccounts[i].name === $scope.model.name &&
                                        $rootScope.previousAccounts[i].team === $scope.model.team) {
                                        $rootScope.previousAccounts.splice(i, 1); // remove it
                                    }
                                }
                                $rootScope.previousAccounts.push(loginObj);
                                cookieService.createCookie($rootScope.prevLoginsCookieName, angular.toJson($rootScope.previousAccounts), 30);
                            }

                        } else {
                            console.error("Received status message for wrong room");
                        }
                        break;
                    case 'USER_PROFILE':
                        for (var i = 0; i < $scope.room.members.length; i++) {
                            if ($scope.room.members[i].name === message.name) {
                                if (message.userPic === null) {
                                    message.userPic = "/res/img/user.png";
                                }
                                $scope.room.members[i] = message;
                                break;
                            }
                        }
                        break;
                    case 'FILE_SHARING_INVITE':
                        $scope.filesharingInvites.pending.push({user: message.sender, sessionId: message.sessionId});
                        break;
                    case 'FILE_SHARING_INVITE_ACCEPT':
                        var session = {user: message.sender, sessionId: message.sessionId, isAccepted: false, isConnected: false, connection: undefined}
                        removePendingInvite(session.sessionId);
                        openFileSharingConnection(session);
                        $scope.filesharingInvites.sessions.push(session);
                        break;
                    case 'FILE_SHARING_INVITE_DECLINE':
                        var sessions = $scope.filesharingInvites.sessions;
                        removePendingInvite(message.sessionId);
                        for (var i = 0; i < sessions.length; i++) {
                            if (sessions[i].sessionId === message.sessionId) {
                                disconnectFilesharingSession(sessions[i]);
                                break;
                            }
                        }
                        break;
                    default:
                        $scope.chatMessage(message.sender, date, message);
                }

            } catch (e) {
                console.error("Error parsing JSON: ", responseText);
                throw e;
            }
        };

        chatSocket = atmosphereService.subscribe(chatRequest);

        //input handling + pushing chat messages
        var input = $('#input');
        input.keydown(function (event) {
            var me = this;
            var msg = $(me).val();
            if (msg && msg.length > 0 && event.keyCode === 13) {
                $scope.$apply(function () {
                    console.log('sending');
                    pushToChatSocket({type: 'CHAT', sender: $scope.model.name, message: msg});
                    $(me).val('');
                });
            }
        });

        $scope.getMemberByName = function (name) {
            for (var i = 0; i < $scope.room.members.length; i++) {
                if ($scope.room.members[i].name === name) {
                    return $scope.room.members[i];
                }
            }
        }

        $scope.addLinkToTweet = function(tweetLinkName, tweetLinkHref) {
            var beginWith = $scope.tweet ? $scope.tweet : "";
            $scope.tweet = beginWith + "<a href='" + tweetLinkHref + "'>" + tweetLinkName + "</a>";
        }

        $scope.toDateString = function (time) {
            var now = new Date();
            if (typeof time === "string") {
                time = parseInt(time);
            }
            var date = new Date(time);
            var diffInMinutes = ((now - date) / 1000) / 60;
            var diffInHours = diffInMinutes / 60;
            if (diffInMinutes < 1) {
                return "just now";
            } else if (diffInHours < 1) {
                return Math.round(diffInMinutes) + " minutes ago";
            } else if (diffInHours < 24) {
                return Math.round(diffInHours) + " hours ago"
            } else {
                return date.toLocaleDateString();
            }
        }

        $scope.sendGetUserProfileMessage = function (userName) {
            pushToChatSocket({type: 'USER_PROFILE', sender: $scope.model.name, team: $scope.model.team, userName: userName});
        }

        $scope.isAtLeastOneFilesharingConnectionActive = function() {
            if ($scope.filesharingInvites.sessions.length > 0) {
                var isAccepted = function (inv) {
                    return inv.isAccepted;
                };
                if ($scope.filesharingInvites.sessions.some(isAccepted)) {
                    return true;
                }
            }
            return false;
        }

        $scope.sendFileSharingInvite = function (toUser) {
            var sessionId = new Date().getTime();
            $scope.filesharingInvites.pendingOutgoing.push({user: toUser, sessionId: sessionId})
            pushToChatSocket({type: 'FILE_SHARING_INVITE', sender: $scope.model.name, toUser: toUser, sessionId: sessionId});
        }

        $scope.acceptFileSharingInvite = function (invite) {
            pushToChatSocket({type: 'FILE_SHARING_INVITE_ACCEPT', sender: $scope.model.name, toUser: invite.user, sessionId: invite.sessionId});
            removePendingInvite(invite.sessionId);
            var session = {user: invite.user, sessionId: invite.sessionId, isAccepted: false, isConnected: false, connection: undefined}
            $scope.filesharingInvites.sessions.push(session);
            joinFileSharingConnection(session)
        }

        $scope.declineFileSharingInvite = function (toUser, sessionId) {
            removePendingInvite(sessionId);
            pushToChatSocket({type: 'FILE_SHARING_INVITE_DECLINE', sender: $scope.model.name, toUser: toUser, sessionId: sessionId});
        }
        
        $scope.closeFilesharingConnection = function (session) {
            $scope.declineFileSharingInvite(session.user, session.sessionId);
            disconnectFilesharingSession(session);
        }

        function disconnectFilesharingSession(session) {
            console.log("disconnecting file sharing session " + session.sessionId)
            session.isAccepted = false;
            session.isConnected = false;
            session.connection.close();
            session.connection.disconnect();
        }

        function removePendingInvite(sessionId) {
            var pending = $scope.filesharingInvites.pending;
            for (var i = 0; i < pending.length; i++) {
                if (pending[i].sessionId == sessionId) {
                    pending.splice(i, 1);
                    break;
                }
            }
            var pendingOut = $scope.filesharingInvites.pendingOutgoing;
            for (var i = 0; i < pendingOut.length; i++) {
                if (pendingOut[i].sessionId == sessionId) {
                    pendingOut.splice(i, 1);
                    break;
                }
            }
        }
//      ================================================================================================================================
//      AVATAR UPLOAD                                                     (https://github.com/kartik-v/bootstrap-fileinput)
//      ================================================================================================================================
        var uploadAvatarInput = $("#upload-avatar-input");
        uploadAvatarInput.fileinput({
            'uploadUrl': "/userPic/upload",
            'uploadExtraData': {userName: $scope.model.name},
            'showUploadedThumbs': false,
            'allowedFileExtensions': ['jpg', 'gif', 'png', 'jpeg', 'bmp'],
            'previewSettings': {image: {width: "100px", height: "100px"}},
            'maxFileSize': 128,
            'maxFileCount': 1,
            'autoReplace': true,
            'dropZoneEnabled': false, //drag and drop support
            'showRemove': false
        });
        uploadAvatarInput.on('fileuploaded', function (event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra,
                response = data.response, reader = data.reader;
            $scope.sendLogInMessage();
        });

        //      ================================================================================================================================

        //document.getElementById('channel').value = $rootScope.team;

        //document.getElementById('init-RTCMultiConnection').onclick = function() {
        //
        //    //document.getElementById('join-RTCMultiConnection').disabled = true;
        //    //document.getElementById('init-RTCMultiConnection').disabled = true;
        //};
        //document.getElementById('join-RTCMultiConnection').onclick = function() {
        //
        //    document.getElementById('join-RTCMultiConnection').disabled = true;
        //    document.getElementById('init-RTCMultiConnection').disabled = true;
        //};

        function openFileSharingConnection(session) {
            session.isAccepted = true;
            var connection = createFilesharingConnection(getOnOpenFunc(session))
            connection.open(session.sessionId);
            session.connection = connection;
            return connection;
        }

        function joinFileSharingConnection(session) {
            session.isAccepted = true;
            var connection = createFilesharingConnection(getOnOpenFunc(session), getOnCloseFunc(session))
            connection.connect(session.sessionId);
            session.connection = connection;
            return connection;
        }

        function registerFilesharingHandlers(session) {
            document.getElementById('file-' + session.sessionId).onchange = function () {
                var file = this.files[0];
                session.connection.send(file);
            };
            session.connection.body = document.getElementById('file-progress-' + session.sessionId);
        }

        function getOnOpenFunc(session) {
            return function () {
                registerFilesharingHandlers(session);
                $scope.$apply(function () {
                    session.isConnected = true;
                });
            }
        }

        function getOnCloseFunc(session) {
            return function () {
                $scope.$apply(function () {
                    session.isConnected = false;
                    session.isAccepted = false;
                });
            }
        }

        function createFilesharingConnection(onOpen, onClose) {
            var connection = new RTCMultiConnection();
            connection.openSignalingChannel = function (callback) {
                var websocket = new WebSocket(filesharingSignalingUrl);

                // data from "onmessage" must be passed over "callback" object
                websocket.onmessage = function (e) {
                    var msg = JSON.parse(e.data);
                    callback.onmessage(msg);
                };

                websocket.onopen = function () {
                    callback.onopen(websocket);
                };
                websocket.actualSend = websocket.send;
                websocket.send = function (msg) {
                    websocket.actualSend(JSON.stringify(msg));
                };
                // socket object must be returned for reusability
                return websocket;
            };
            connection.session = {
                data: true
            };
            // [optional] onmessage/onopen is for sending/receiving data/text
            //connection.onmessage = function(e) {
            //
            //};
            connection.onopen = function() {
                onOpen();
            };
            connection.onclose = function() {
                onClose();
            };
            return connection;
        }

    }]);
//      ================================================================================================================================

// this directive is needed to handle file from upload box (binds it to current $scope)
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.directive('showtab',
    function () {
        return {
            link: function (scope, element, attrs) {
                element.click(function (e) {
                    e.preventDefault();
                    $(element).tab('show');
                });
            }
        };
    });

app.directive('dropdown',
    function () {
        return {
            link: function (scope, element, attrs) {
                element.click(function (e) {
                    $(element).dropdown();
                });
            }
        };
    });
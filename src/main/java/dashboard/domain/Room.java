package dashboard.domain;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.util.Assert;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import dashboard.actor.BroadcastingActor;
import dashboard.actor.DbActor;
import dashboard.actor.RoomPubsubDispatcherActor;
import dashboard.actor.message.GetTeamStatus;
import dashboard.actor.message.Logout;
import dashboard.actor.message.PrivateMessage;
import dashboard.actor.message.Subscribe;
import dashboard.actor.message.Unsubscribe;
import dashboard.configuration.akka.SpringExtension;
import dashboard.websockets.message.AbstractWebsocketMessage;
import dashboard.websockets.message.ChatMessage;
import dashboard.websockets.message.Direction;
import dashboard.websockets.message.LoginMessage;
import dashboard.websockets.message.TeamInfoMessage;
import dashboard.websockets.message.TweetMessage;
import dashboard.websockets.message.UserInfoMessage;
import dashboard.websockets.message.UserProfileRequestMessage;

import static akka.actor.ActorRef.noSender;

/**
 * Created by vx on 7/25/2015.
 */
public class Room {

	private String roomName;
	private Map<String, RoomMember> members = new ConcurrentHashMap<String, RoomMember>();

	private ActorSystem actorSystem;
	private ActorRef roomPubsubDispatcher;
	private ActorRef dbActor;

	public Room(String roomName, ActorSystem actorSystem) {
		this.roomName = roomName;
		this.actorSystem = actorSystem;
		this.roomPubsubDispatcher = SpringExtension.createSpringActor(actorSystem, RoomPubsubDispatcherActor.BEAN, "roomDispatcher-" + roomName);
		this.dbActor = SpringExtension.createSpringActor(actorSystem, DbActor.BEAN);

		roomPubsubDispatcher.tell(new Subscribe(dbActor, getPersistenceSubscriptions()), noSender());
	}

	private Class[] getPersistenceSubscriptions() {
		return new Class[] {
				GetTeamStatus.class,
				LoginMessage.class,
				Logout.class,
				TweetMessage.class,
				UserProfileRequestMessage.class,
				ChatMessage.class
		};
	}

	private Class[] getMemberSubscriptions() {
		return new Class[] {
				ChatMessage.class,
				TweetMessage.class,
				TeamInfoMessage.class,
				UserInfoMessage.class
		};
	}

	/**
	 * @return false if was already registered
	 */
	public boolean registerBroadcaster(RoomMember member) {
		Assert.isTrue(roomName.equals(member.getRoomName()), "whoops, tried to register user with the wrong team");
		RoomMember registered = members.get(member.getUserName());
		boolean existed = registered != null;

		//kill previous bro if alive (no duplicate login allowed)
		if (existed && registered.getBroadcastingActor() != null) {
			registered.getBroadcastingActor().tell(PoisonPill.getInstance(), noSender());
		}
		String broActorName = "broadcaster-" + member.getRoomName() + "-" + member.getUserName() + "-" + System.currentTimeMillis();
		Object[] actorConstructorArgs = { member.getRoomName() + "-" + member.getUserName(), member.getBroadcaster() };
		ActorRef broadcastingActor = SpringExtension.createSpringActor(actorSystem, BroadcastingActor.BEAN, broActorName, actorConstructorArgs);

		member.setBroadcastingActor(broadcastingActor);

		roomPubsubDispatcher.tell(new Subscribe.Membership(member), noSender());
		roomPubsubDispatcher.tell(new Subscribe(broadcastingActor, getMemberSubscriptions()), noSender());

		members.put(member.getUserName(), member);

		return existed;
	}

	public void broadcastRoomStatus(RoomMember member) {
		//save if not exist
		roomPubsubDispatcher.tell(new GetTeamStatus(roomName), noSender());
	}

	public void broadcast(AbstractWebsocketMessage message) {
		roomPubsubDispatcher.tell(message, noSender());
	}

	public void privateBroadcast(String userName, AbstractWebsocketMessage message) {
		message.setDirection(Direction.TO_CLIENT);
		roomPubsubDispatcher.tell(new PrivateMessage(roomName, userName, message), noSender());
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Room{");
		sb.append("roomName='").append(roomName).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(roomName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final Room other = (Room) obj;
		return Objects.equals(this.roomName, other.roomName);
	}

	public void logIn(LoginMessage message) {
		roomPubsubDispatcher.tell(message, noSender());
	}

	public void logOut(String userName, String teamName) {
		roomPubsubDispatcher.tell(new Logout(userName, teamName), noSender());
		RoomMember member = members.get(userName);
		// kill the bro :(
		roomPubsubDispatcher.tell(new Unsubscribe(member.getBroadcastingActor(), member, getMemberSubscriptions()), noSender());
		member.getBroadcastingActor().tell(PoisonPill.getInstance(), noSender());
		member.setBroadcastingActor(null);

	}

	public void propagateAndPersistTweet(TweetMessage message) {
		sendToClientThroughDispatcher(message);
	}

	public void broadcastUserData(UserProfileRequestMessage message) {
		roomPubsubDispatcher.tell(message, noSender());
	}

	public void propagateAndPersistChatMessage(ChatMessage message) {
		sendToClientThroughDispatcher(message);
	}

	private void sendToClientThroughDispatcher(AbstractWebsocketMessage message) {
		message.setDirection(Direction.TO_CLIENT);
		roomPubsubDispatcher.tell(message, noSender());
	}
}

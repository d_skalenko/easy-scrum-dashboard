package dashboard.domain;

import java.util.Objects;

import org.atmosphere.cpr.Broadcaster;

import akka.actor.ActorRef;

/**
 * Created by vx on 8/29/2015.
 */
public class RoomMember {

	private String roomName;
	private String userName;
	private Broadcaster broadcaster;
	private ActorRef broadcastingActor;

	public RoomMember(String roomName, String userName, Broadcaster broadcaster) {
		this.roomName = roomName;
		this.userName = userName;
		this.broadcaster = broadcaster;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Broadcaster getBroadcaster() {
		return broadcaster;
	}

	public void setBroadcaster(Broadcaster broadcaster) {
		this.broadcaster = broadcaster;
	}

	public ActorRef getBroadcastingActor() {
		return broadcastingActor;
	}

	public void setBroadcastingActor(ActorRef broadcastingActor) {
		this.broadcastingActor = broadcastingActor;
	}

	@Override
	public int hashCode() {
		return Objects.hash(roomName, userName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final RoomMember other = (RoomMember) obj;
		return Objects.equals(this.roomName, other.roomName) && Objects.equals(this.userName, other.userName);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("RoomMember{");
		sb.append("roomName='").append(roomName).append('\'');
		sb.append(", userName='").append(userName).append('\'');
//		sb.append(", broadcaster=").append(broadcaster);
		sb.append('}');
		return sb.toString();
	}
}

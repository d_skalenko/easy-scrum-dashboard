package dashboard.actor;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

import akka.actor.UntypedActor;
import dashboard.actor.message.GetTeamStatus;
import dashboard.actor.message.Logout;
import dashboard.actor.message.PrivateMessage;
import dashboard.persistance.dao.TeamDao;
import dashboard.persistance.dao.TweetDao;
import dashboard.persistance.dao.UserDao;
import dashboard.persistance.entity.StatusTweet;
import dashboard.persistance.service.ChatLogsService;
import dashboard.persistance.service.PersistenceService;
import dashboard.websockets.message.ChatMessage;
import dashboard.websockets.message.HistoMessage;
import dashboard.websockets.message.LoginMessage;
import dashboard.websockets.message.MessageType;
import dashboard.websockets.message.TeamInfoMessage;
import dashboard.websockets.message.TweetMessage;
import dashboard.websockets.message.UserInfoMessage;
import dashboard.websockets.message.UserProfileRequestMessage;

/**
 * Created by vx on 8/2/2015.
 */
@Component(DbActor.BEAN)
@Scope("prototype")
public class DbActor extends UntypedActor {

	public static final String BEAN = "dbActor";
	public static final int TWEETS_PAGE_SIZE = 20;

	@Autowired
	private TeamDao teamDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private TweetDao tweetDao;

	@Autowired
	private PersistenceService persistenceService;

	@Autowired
	private ChatLogsService chatLogsService;

	@Override
	public void onReceive(Object message) throws Exception {
//-------------------------------------------------------------------------------------------------
		if (message instanceof GetTeamStatus) {
			GetTeamStatus teamStatus = (GetTeamStatus) message;
			persistenceService.getOrCreateTeam(teamStatus.getTeamName());
		} else if (message instanceof LoginMessage) {
			LoginMessage loginMessage = (LoginMessage) message;
			TeamInfoMessage statusAfterLogin = new TeamInfoMessage(persistenceService.logInUser(loginMessage));
			Page<StatusTweet> lastTweets = tweetDao.findByTeam(loginMessage.getTeam(),
					new PageRequest(0, TWEETS_PAGE_SIZE, new Sort(new Sort.Order(Sort.Direction.DESC, "id"))));
			statusAfterLogin.convertAndSetTweets(lastTweets.getContent());
			sender().tell(statusAfterLogin, self());

			HistoMessage histo = new HistoMessage(chatLogsService.getRecentLogs(loginMessage.getTeam()));
			PrivateMessage msg = new PrivateMessage(loginMessage.getTeam(), loginMessage.getSender(), histo);
			sender().tell(msg, self());
		} else if (message instanceof Logout) {
			Logout logout = (Logout) message;
			persistenceService.logOutUser(logout.getUserName());
			sender().tell(new ChatMessage(logout.getUserName() + " has disconnected!", logout.getUserName(), MessageType.DISCONNECTED), self());
		} else if (message instanceof TweetMessage) {
			TweetMessage tweetMessage = (TweetMessage) message;
			persistenceService.persistTweet(tweetMessage);
		} else if (message instanceof UserProfileRequestMessage) {
			final UserProfileRequestMessage userProfileRequestMessage = (UserProfileRequestMessage) message;
			UserInfoMessage userInfoMessage = new UserInfoMessage(persistenceService.getUserProfile(userProfileRequestMessage));
			ArrayList<TweetMessage> filtered =
					Lists.newArrayList(Collections2.filter(userInfoMessage.getTweets(), new Predicate<TweetMessage>() {
						@Override
						public boolean apply(TweetMessage input) {
							return input.getTeam().equals(userProfileRequestMessage.getTeam());
						}
					}));
			userInfoMessage.setTweets(filtered);
			userInfoMessage.setSender("SERVER");
			userInfoMessage.setType(MessageType.USER_PROFILE);
			sender().tell(userInfoMessage, self());
		} else if (message instanceof ChatMessage) {
			ChatMessage chatMessage = (ChatMessage) message;
			if (chatMessage.getType().equals(MessageType.CHAT)) {
				chatLogsService.persistMessage(chatMessage);
			}
		}
	}
}

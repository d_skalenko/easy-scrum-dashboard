package dashboard.actor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.google.common.collect.Sets;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import dashboard.actor.message.PrivateMessage;
import dashboard.actor.message.Subscribe;
import dashboard.actor.message.Unsubscribe;
import dashboard.domain.RoomMember;

@Component(RoomPubsubDispatcherActor.BEAN)
@Scope("prototype")
public class RoomPubsubDispatcherActor extends UntypedActor {

	public static final String BEAN = "roomPubsubDispatcherActor";

	private Map<Class, Set<ActorRef>> subscribers = new HashMap<Class, Set<ActorRef>>();
	private Map<String, RoomMember> members = new HashMap<String, RoomMember>();

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof Subscribe) {
			Subscribe msg = (Subscribe) message;
			for (Class clazz : msg.getMessageClasses()) {
				subscribe(clazz, msg.getSubscriber());
			}

		}  else if (message instanceof Subscribe.Membership) {
			Subscribe.Membership msg = (Subscribe.Membership) message;
			members.put(msg.getMember().getUserName(), msg.getMember());

		}  else if (message instanceof Unsubscribe) {
			Unsubscribe msg = (Unsubscribe) message;
			for (Class clazz : msg.getMessageClasses()) {
				unsubscribe(clazz, msg.getSubscriber());
			}

		} else if (message instanceof PrivateMessage) {
			PrivateMessage msg = (PrivateMessage) message;
			RoomMember member = members.get(msg.getUserName());
			Assert.notNull(member, "Unknown member " + msg.getUserName());
			ActorRef broadcastingActor = member.getBroadcastingActor();
			if (broadcastingActor == null) {
				System.err.println("THIS SHOULD NEVER HAPPEN: bradcastingActor is null for member " + member.getUserName());
			} else {
				broadcastingActor.tell(msg.getMessage(), self());
			}

		} else {
			Set<ActorRef> actorRefs = subscribers.get(message.getClass());
			if (actorRefs != null) {
				for (ActorRef actor : actorRefs) {
					actor.tell(message, self());
				}
			}
		}
	}

	@Override
	public void unhandled(Object message) {
		System.out.println("Unhandled message " + message);
	}

	private void subscribe(Class topic, ActorRef ref) {
		Set<ActorRef> topicRefs = subscribers.get(topic);
		if (topicRefs == null) {
			subscribers.put(topic, Sets.newHashSet(ref));
		} else {
			topicRefs.add(ref);
		}
	}

	private void unsubscribe(Class topic, ActorRef ref) {
		Set<ActorRef> topicRefs = subscribers.get(topic);
		if (topicRefs == null) {
			// this might be a leak
			System.err.println("LEAK CHECK: " + topic + " -- " + ref + " should unsubscribe from -- " + subscribers);
		}
		if (topicRefs != null) {
			topicRefs.remove(ref);
		}
	}
}

package dashboard.actor.message;

/**
 * Created by vx on 8/16/2015.
 */
public class Logout {

	private String userName;

	private String teamName;

	public Logout(String userName, String teamName) {
		this.userName = userName;
		this.teamName = teamName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
}

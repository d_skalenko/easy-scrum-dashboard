package dashboard.actor.message;

import akka.actor.ActorRef;
import dashboard.domain.RoomMember;

public class Subscribe {

	private final Class[] messageClasses;
	private final ActorRef subscriber;

	public Subscribe(ActorRef subscriber, Class... messageClasses) {
		this.messageClasses = messageClasses;
		this.subscriber = subscriber;
	}

	public Class[] getMessageClasses() {
		return messageClasses;
	}

	public ActorRef getSubscriber() {
		return subscriber;
	}

	public static class Membership {

		private RoomMember member;

		public Membership(RoomMember member) {
			this.member = member;
		}

		public RoomMember getMember() {
			return member;
		}
	}
}

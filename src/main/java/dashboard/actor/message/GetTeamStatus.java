package dashboard.actor.message;

/**
 * Creates team if it doesn't exist yet
 * Created by vx on 8/2/2015.
 */
public class GetTeamStatus {
	private String teamName;

	public GetTeamStatus(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
}

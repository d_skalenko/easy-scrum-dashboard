package dashboard.actor.message;

import akka.actor.ActorRef;
import dashboard.domain.RoomMember;

public class Unsubscribe {

	private final Class[] messageClasses;
	private final ActorRef subscriber;
	private final RoomMember member;

	public Unsubscribe(ActorRef subscriber, RoomMember member, Class... messageClasses) {
		this.member = member;
		this.messageClasses = messageClasses;
		this.subscriber = subscriber;
	}

	public Class[] getMessageClasses() {
		return messageClasses;
	}

	public ActorRef getSubscriber() {
		return subscriber;
	}

	public RoomMember getMember() {
		return member;
	}
}

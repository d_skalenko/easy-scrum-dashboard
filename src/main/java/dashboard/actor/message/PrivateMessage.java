package dashboard.actor.message;

/**
 * Created by vx on 8/29/2015.
 */
public class PrivateMessage {

	private String roomName;
	private String userName;
	private Object message;

	public PrivateMessage(String roomName, String userName, Object message) {
		this.roomName = roomName;
		this.userName = userName;
		this.message = message;
	}

	public String getRoomName() {
		return roomName;
	}

	public String getUserName() {
		return userName;
	}

	public Object getMessage() {
		return message;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PrivateMessage{");
		sb.append("roomName='").append(roomName).append('\'');
		sb.append(", userName='").append(userName).append('\'');
		sb.append(", message=").append(message);
		sb.append('}');
		return sb.toString();
	}
}

package dashboard.actor;

import org.atmosphere.cpr.Broadcaster;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import akka.actor.UntypedActor;

@Component(BroadcastingActor.BEAN)
@Scope("prototype")
public class BroadcastingActor extends UntypedActor {

	public static final String BEAN = "broadcastingActor";

	private final String channel;
	private final Broadcaster websocketsBroadcaster;

	public BroadcastingActor(String channel, Broadcaster websocketsBroadcaster) {
		this.channel = channel;
		this.websocketsBroadcaster = websocketsBroadcaster;
	}

	@Override
	public void onReceive(Object message) throws Exception {
		System.out.println("actor " + Thread.currentThread().getName() + " broadcasting " + message);
		websocketsBroadcaster.broadcast(message);
	}

	@Override
	public void unhandled(Object message) {
		System.out.println("Unhandled message " + message);
	}

}

package dashboard;

import java.util.Collections;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.cpr.AtmosphereServlet;
import org.atmosphere.cpr.ContainerInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class EasyScrumDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyScrumDashboardApplication.class, args);
	}

	@Bean
	public EmbeddedAtmosphereInitializer atmosphereInitializer() {
		return new EmbeddedAtmosphereInitializer();
	}

	@Bean
	public ServletRegistrationBean atmosphereServlet() {
		ServletRegistrationBean registration = new ServletRegistrationBean(
				new AtmosphereServlet(), "/ws/*");
		registration.addInitParameter(ApplicationConfig.ANNOTATION_PACKAGE, "dashboard.websockets.service");
		registration.addInitParameter(ApplicationConfig.OBJECT_FACTORY, "org.atmosphere.spring.SpringWebObjectFactory");
		registration.addInitParameter(ApplicationConfig.HEARTBEAT_INTERVAL_IN_SECONDS, "120");
		registration.setLoadOnStartup(0);
		// Need to occur before the EmbeddedAtmosphereInitializer
		registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return registration;
	}

	private static class EmbeddedAtmosphereInitializer extends ContainerInitializer implements ServletContextInitializer {
		@Override
		public void onStartup(ServletContext servletContext) throws ServletException {
			onStartup(Collections.<Class<?>>emptySet(), servletContext);
		}

	}

}

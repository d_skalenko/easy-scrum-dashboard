package dashboard.websockets.serializer;

import com.fasterxml.jackson.core.type.TypeReference;

import dashboard.websockets.message.UserInfoMessage;

/**
 * Created by vx on 8/22/2015.
 */
public class UserInfoMessageSerializer extends AbstractSerializer<UserInfoMessage> {
	public UserInfoMessageSerializer() {
		super(new TypeReference<UserInfoMessage>() {
		});
	}
}

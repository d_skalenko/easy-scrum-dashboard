package dashboard.websockets.serializer;

import dashboard.websockets.message.ChatMessage;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Created by vx on 7/25/2015.
 */
public class ChatMessageSerializer extends AbstractSerializer<ChatMessage> {

	public ChatMessageSerializer() {
		super(new TypeReference<ChatMessage>() {});
	}
}

package dashboard.websockets.serializer;

import java.io.IOException;

import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by vx on 7/25/2015.
 */
public class AbstractSerializer<T> implements Encoder<T, String>, Decoder<String, T> {

	public final TypeReference VALUE_TYPE_REF;
	private final ObjectMapper mapper = new ObjectMapper();

	public AbstractSerializer(TypeReference value_type_ref) {
		VALUE_TYPE_REF = value_type_ref;
	}

	@Override
	public T decode(String s) {
		try {
			return this.mapper.readValue(s, VALUE_TYPE_REF);
		}
		catch (IOException ex) {

			throw new IllegalStateException(ex);
		}
	}

	@Override
	public String encode(T s) {
		try {
			return this.mapper.writeValueAsString(s);
		}
		catch (IOException ex) {
			throw new IllegalStateException(ex);
		}
	}
}

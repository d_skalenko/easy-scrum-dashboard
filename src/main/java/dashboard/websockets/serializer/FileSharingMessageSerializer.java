package dashboard.websockets.serializer;

import com.fasterxml.jackson.core.type.TypeReference;

import dashboard.websockets.message.FileSharingMessage;

/**
 * Created by vx on 8/22/2015.
 */
public class FileSharingMessageSerializer extends AbstractSerializer<FileSharingMessage> {

	public FileSharingMessageSerializer() {
		super(new TypeReference<FileSharingMessage>() {});
	}
}
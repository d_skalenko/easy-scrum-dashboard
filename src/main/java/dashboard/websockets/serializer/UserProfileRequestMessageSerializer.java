package dashboard.websockets.serializer;

import com.fasterxml.jackson.core.type.TypeReference;

import dashboard.websockets.message.UserProfileRequestMessage;

/**
 * Created by vx on 8/22/2015.
 */
public class UserProfileRequestMessageSerializer extends AbstractSerializer<UserProfileRequestMessage> {

	public UserProfileRequestMessageSerializer() {
		super(new TypeReference<UserProfileRequestMessage>() {
		});
	}
}
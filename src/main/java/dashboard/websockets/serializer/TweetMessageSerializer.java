package dashboard.websockets.serializer;

import com.fasterxml.jackson.core.type.TypeReference;

import dashboard.websockets.message.TweetMessage;

/**
 * Created by vx on 8/15/2015.
 */
public class TweetMessageSerializer extends AbstractSerializer<TweetMessage> {
	public TweetMessageSerializer() {
		super(new TypeReference<TweetMessage>() {});
	}
}

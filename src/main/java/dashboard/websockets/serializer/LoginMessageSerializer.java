package dashboard.websockets.serializer;

import com.fasterxml.jackson.core.type.TypeReference;

import dashboard.websockets.message.LoginMessage;

/**
 * Created by vx on 8/3/2015.
 */
public class LoginMessageSerializer extends AbstractSerializer<LoginMessage> {
	public LoginMessageSerializer() {
		super(new TypeReference<LoginMessage>() {});
	}
}

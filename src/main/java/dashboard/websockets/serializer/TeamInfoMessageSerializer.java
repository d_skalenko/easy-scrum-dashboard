package dashboard.websockets.serializer;

import com.fasterxml.jackson.core.type.TypeReference;

import dashboard.websockets.message.TeamInfoMessage;

/**
 * Created by vx on 8/3/2015.
 */
public class TeamInfoMessageSerializer extends AbstractSerializer<TeamInfoMessage> {
	public TeamInfoMessageSerializer() {
		super(new TypeReference<TeamInfoMessage>() {});
	}
}

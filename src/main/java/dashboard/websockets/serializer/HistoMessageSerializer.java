package dashboard.websockets.serializer;

import dashboard.websockets.message.HistoMessage;

import com.fasterxml.jackson.core.type.TypeReference;

public class HistoMessageSerializer extends AbstractSerializer<HistoMessage> {
	public HistoMessageSerializer() {
		super(new TypeReference<HistoMessage>() {});
	}
}

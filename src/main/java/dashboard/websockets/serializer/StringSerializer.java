package dashboard.websockets.serializer;

import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;

/**
 * Created by vx on 2/15/2016.
 */
public class StringSerializer implements Encoder<String, String>, Decoder<String, String> {

	@Override
	public String decode(String s) {
		return s;
	}

	@Override
	public String encode(String s) {
		return s;
	}
}

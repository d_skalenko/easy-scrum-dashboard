package dashboard.websockets.message;

public enum MessageType {
	SYSTEM, //			anything critical
	LOGGED_IN, //
	DISCONNECTED, //
	CHAT, //			public chat messages
	TYPING, //			someone is typing
	STOPPED_TYPING, //  stopped
	PRIVATE, //			private message
	STATUS, //			room status(users offline/online)
	USER_PROFILE, //			request/response to retrieve profile
	TWEET, //			status tweet
	CHAT_HISTO,
	FILE_SHARING_INVITE,
	FILE_SHARING_INVITE_ACCEPT,
	FILE_SHARING_INVITE_DECLINE
}

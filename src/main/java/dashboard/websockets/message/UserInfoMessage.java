package dashboard.websockets.message;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.LazyInitializationException;

import dashboard.persistance.entity.StatusTweet;
import dashboard.persistance.entity.TeamInfo;
import dashboard.persistance.entity.User;

/**
 * Created by vx on 8/15/2015.
 */
public class UserInfoMessage extends AbstractWebsocketMessage{

	private long id;
	private String name;
	private String role;
	private String userPic;
	private boolean online = false;
	private Set<String> teams;
	private List<TweetMessage> tweets;

	public UserInfoMessage() {
	}

	public UserInfoMessage(String name, String role) {
		this.name = name;
		this.role = role;
		setDirection(Direction.TO_CLIENT);
	}

	public UserInfoMessage(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.role = user.getRole();
		this.userPic = user.getUserPic();
		this.online = user.isOnline();
		Set<String> teams = new HashSet<String>();
		for (TeamInfo teamInfo : user.getTeams()) {
			teams.add(teamInfo.getName());
		}
		this.teams = teams;

		List<TweetMessage> tweets = new ArrayList<TweetMessage>();
		try {
			for (StatusTweet tweet : user.getTweets()) {
				tweets.add(new TweetMessage(tweet));
			}
		}
		catch (LazyInitializationException e) {

		}
		this.tweets = tweets;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserPic() {
		return userPic;
	}

	public void setUserPic(String userPic) {
		this.userPic = userPic;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public Set<String> getTeams() {
		return teams;
	}

	public void setTeams(Set<String> teams) {
		this.teams = teams;
	}

	public List<TweetMessage> getTweets() {
		return tweets;
	}

	public void setTweets(List<TweetMessage> tweets) {
		this.tweets = tweets;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("UserInfoMessage{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", role='").append(role).append('\'');
		sb.append(", userPic='").append(userPic).append('\'');
		sb.append(", online=").append(online);
		sb.append(", teams=").append(teams);
		sb.append(", tweets=").append(tweets);
		sb.append('}');
		return sb.toString();
	}

}

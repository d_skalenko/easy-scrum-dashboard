package dashboard.websockets.message;

public class ChatMessage extends AbstractWebsocketMessage {

	private String message;

	private String receiver;

	public ChatMessage() {
	}

	public ChatMessage(String message, String sender, MessageType type) {
		this.message = message;
		setSender(sender);
		setType(type);
		setDirection(Direction.TO_CLIENT);
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ChatMessage{");
		sb.append("message='").append(message).append('\'');
		sb.append(", receiver='").append(receiver).append('\'');
		sb.append('}');
		return sb.toString();
	}
}

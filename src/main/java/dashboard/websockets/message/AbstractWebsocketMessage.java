package dashboard.websockets.message;

import java.util.Date;

public abstract class AbstractWebsocketMessage {

	private Direction direction;

	private MessageType type;

	private String sender;

	private long time = new Date().getTime();

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("AbstractWebsocketMessage{");
		sb.append("direction=").append(direction);
		sb.append(", type=").append(type);
		sb.append(", sender='").append(sender).append('\'');
		sb.append(", time=").append(time);
		sb.append('}');
		return sb.toString();
	}
}

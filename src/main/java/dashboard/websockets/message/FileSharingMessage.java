package dashboard.websockets.message;

/**
 * Created by vx on 2/26/2016.
 */
public class FileSharingMessage extends AbstractWebsocketMessage {
	private String toUser;
	private String sessionId;

	public FileSharingMessage() {
	}

	public FileSharingMessage(String toUser, String sessionId) {
		this.toUser = toUser;
		this.sessionId = sessionId;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("FileSharingMessage{");
		sb.append("super='").append(super.toString()).append('\'');
		sb.append("toUser='").append(toUser).append('\'');
		sb.append(", sessionId='").append(sessionId).append('\'');
		sb.append('}');
		return sb.toString();
	}
}

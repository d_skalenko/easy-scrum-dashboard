package dashboard.websockets.message;

import java.util.List;

public class HistoMessage extends AbstractWebsocketMessage {

	private List<AbstractWebsocketMessage> histo;

	public HistoMessage(List<AbstractWebsocketMessage> histo) {
		setType(MessageType.CHAT_HISTO);
		setDirection(Direction.TO_CLIENT);
		this.histo = histo;
	}

	public List<AbstractWebsocketMessage> getHisto() {
		return histo;
	}

	public void setHisto(List<AbstractWebsocketMessage> histo) {
		this.histo = histo;
	}
}

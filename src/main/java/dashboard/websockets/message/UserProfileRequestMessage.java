package dashboard.websockets.message;

/**
 * Created by vx on 8/22/2015.
 */
public class UserProfileRequestMessage extends AbstractWebsocketMessage {

	private String userName;
	private String team;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("UserProfileRequestMessage{");
		sb.append("userName='").append(userName).append('\'');
		sb.append(", team='").append(team).append('\'');
		sb.append('}');
		return sb.toString();
	}
}

package dashboard.websockets.message;

/**
 * Created by vx on 8/3/2015.
 */
public class LoginMessage extends AbstractWebsocketMessage {

	private String team;

	private String uuid; //	Atmosphere resource UUID

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("LoginMessage{");
		sb.append("team='").append(team).append('\'');
		sb.append('}');
		return sb.toString();
	}
}

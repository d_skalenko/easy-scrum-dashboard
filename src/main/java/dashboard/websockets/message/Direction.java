package dashboard.websockets.message;

/**
 * Created by vx on 8/30/2015.
 */
public enum Direction {
	TO_SERVER,
	TO_CLIENT
}

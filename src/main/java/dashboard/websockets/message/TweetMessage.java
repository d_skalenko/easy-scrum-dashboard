package dashboard.websockets.message;

import dashboard.persistance.entity.StatusTweet;

import java.util.Date;

/**
 * Created by vx on 8/15/2015.
 */
public class TweetMessage extends AbstractWebsocketMessage {
	private long id;
	private String team;
	private String text;
	private Date date;

	public TweetMessage() {
		// default constructor
	}

	public TweetMessage(StatusTweet tweet) {
		this.id = tweet.getId();
		this.text = tweet.getText();
		setSender(tweet.getSender());
		this.team = tweet.getTeam();
		this.date = tweet.getDate();
		setDirection(Direction.TO_CLIENT);
	}

	public long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("TweetMessage{");
		sb.append(super.toString());
		sb.append(", id=").append(id);
		sb.append(", text='").append(text).append('\'');
		sb.append(", team='").append(team).append('\'');
		sb.append(", date=").append(date);
		sb.append('}');
		return sb.toString();
	}
}

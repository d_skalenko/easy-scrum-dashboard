package dashboard.websockets.message;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.LazyInitializationException;

import com.google.common.collect.Lists;

import dashboard.persistance.entity.StatusTweet;
import dashboard.persistance.entity.TeamInfo;
import dashboard.persistance.entity.User;

/**
 * Created by vx on 8/3/2015.
 */
public class TeamInfoMessage extends AbstractWebsocketMessage {

	private String teamName;

	private List<UserInfoMessage> members;

	private List<TweetMessage> tweets;

	public static final Comparator<UserInfoMessage> USER_INFO_MESSAGE_COMPARATOR = new Comparator<UserInfoMessage>() {
		@Override
		public int compare(UserInfoMessage userInfoMessage, UserInfoMessage userInfoMessage2) {
			return userInfoMessage.getName().compareTo(userInfoMessage2.getName());
		}
	};

	public TeamInfoMessage(TeamInfo teamInfo) {
		this.teamName = teamInfo.getName();
		Set<UserInfoMessage> members = new HashSet<UserInfoMessage>();
		for (User user : teamInfo.getMembers()) {
			members.add(new UserInfoMessage(user));
		}
		ArrayList<UserInfoMessage> userInfoMessages = Lists.newArrayList(members);
		userInfoMessages.sort(USER_INFO_MESSAGE_COMPARATOR);
		this.members = userInfoMessages;
		List<TweetMessage> tweets = new ArrayList<TweetMessage>();
		try {
			for (StatusTweet tweet : teamInfo.getTweets()) {
				tweets.add(new TweetMessage(tweet));
			}
		}
		catch (LazyInitializationException e) {
			//do nothing
		}
		this.tweets = tweets;
		setTime(System.currentTimeMillis());
		setType(MessageType.STATUS);
		setSender("SERVER");
		setDirection(Direction.TO_CLIENT);
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public List<TweetMessage> getTweets() {
		return tweets;
	}

	public void setTweets(List<TweetMessage> tweets) {
		this.tweets = tweets;
	}

	public void convertAndSetTweets(List<StatusTweet> tweets) {
		List<TweetMessage> convertedTweets = new ArrayList<TweetMessage>();
		try {
			for (StatusTweet tweet : tweets) {
				convertedTweets.add(new TweetMessage(tweet));
			}
		}
		catch (LazyInitializationException e) {
			//do nothing
		}
		this.tweets = convertedTweets;
	}

	public List<UserInfoMessage> getMembers() {
		return members;
	}

	public void setMembers(List<UserInfoMessage> members) {
		this.members = members;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("TeamInfoMessage{");
		sb.append("teamName='").append(teamName).append('\'');
		sb.append(", members=").append(members);
		sb.append(", tweets=").append(tweets);
		sb.append('}');
		return sb.toString();
	}
}

package dashboard.websockets.service.dashboard;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.PathParam;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceFactory;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.MetaBroadcaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import dashboard.service.WebsocketMessageHandler;
import dashboard.service.WebsocketMessageHandlerRegistry;
import dashboard.websockets.message.AbstractWebsocketMessage;
import dashboard.websockets.message.ChatMessage;
import dashboard.websockets.message.FileSharingMessage;
import dashboard.websockets.message.HistoMessage;
import dashboard.websockets.message.LoginMessage;
import dashboard.websockets.message.TeamInfoMessage;
import dashboard.websockets.message.TweetMessage;
import dashboard.websockets.message.UserInfoMessage;
import dashboard.websockets.message.UserProfileRequestMessage;
import dashboard.websockets.serializer.ChatMessageSerializer;
import dashboard.websockets.serializer.FileSharingMessageSerializer;
import dashboard.websockets.serializer.HistoMessageSerializer;
import dashboard.websockets.serializer.LoginMessageSerializer;
import dashboard.websockets.serializer.TeamInfoMessageSerializer;
import dashboard.websockets.serializer.TweetMessageSerializer;
import dashboard.websockets.serializer.UserInfoMessageSerializer;
import dashboard.websockets.serializer.UserProfileRequestMessageSerializer;

//TODO: rewrite this garbage into single message handler, with signature {type: "", direction: "", message: ""}
@ManagedService(path = "/ws/room/{roomName: [a-zA-Z][a-zA-Z_0-9]*}/user/{userName: [a-zA-Z][a-zA-Z_0-9]*}")
public class DashboardWebsocketsService extends SpringBeanAutowiringSupport {

	private static final Logger LOGGER = LoggerFactory.getLogger(DashboardWebsocketsService.class);

	// uuid - userName pairs
	private final ConcurrentHashMap<String, String> userResourceIds = new ConcurrentHashMap<String, String>();

	@PathParam("roomName")
	private String roomName;

	@PathParam("userName")
	private String userName;

	@Inject
	private BroadcasterFactory factory;

	@Inject
	private AtmosphereResourceFactory resourceFactory;

	@Inject
	private MetaBroadcaster metaBroadcaster;

	@Autowired
	private WebsocketMessageHandlerRegistry websocketMessageHandlerRegistry;

	private Broadcaster privateChannel;

	/**
	 * When user connects
	 */
	@Ready
	public void onReady(final AtmosphereResource resource) {
		LOGGER.info("Connected {}", resource.uuid());
		LOGGER.info("USER : {}, {}", roomName, userName);
		getHandler().onConnect(resource.getBroadcaster());
	}

	@Disconnect
	public void onDisconnect(AtmosphereResourceEvent event) {
		String uuid = event.getResource().uuid();
		LOGGER.info("Client {} disconnected [{}]", uuid,
				(event.isCancelled() ? "cancelled" : "closed"));
		Collection<WebsocketMessageHandler> userHandlers =
				websocketMessageHandlerRegistry.getUserHandlersByRoom(userResourceIds.get(uuid)).values();
		for (WebsocketMessageHandler handler : userHandlers) {
			handler.onDisconnect();
		}
		userResourceIds.remove(uuid);

	}

	@Message(encoders = ChatMessageSerializer.class, decoders = ChatMessageSerializer.class)
	public AbstractWebsocketMessage onChatMessage(ChatMessage message) throws IOException {
		return getHandler().onChatMessage(message);
	}

	@Message(encoders = LoginMessageSerializer.class, decoders = LoginMessageSerializer.class)
	public AbstractWebsocketMessage onLogin(LoginMessage message) throws IOException {
		LOGGER.info("User {} logged in with uuid {}", message.getSender(), message.getUuid());
		userResourceIds.put(message.getUuid(), message.getSender());
		return getHandler().onLogin(message);
	}

	@Message(encoders = TeamInfoMessageSerializer.class, decoders = TeamInfoMessageSerializer.class)
	public AbstractWebsocketMessage onTeamInfo(TeamInfoMessage message) throws IOException {
		return getHandler().onTeamInfoRequest(message);
	}

	@Message(encoders = TweetMessageSerializer.class, decoders = TweetMessageSerializer.class)
	public AbstractWebsocketMessage onTweet(TweetMessage message) throws IOException {
		return getHandler().onTweet(message);
	}

	@Message(encoders = UserProfileRequestMessageSerializer.class, decoders = UserProfileRequestMessageSerializer.class)
	public AbstractWebsocketMessage onUserProfileRequest(UserProfileRequestMessage message) throws IOException {
		return getHandler().onUserProfileRequest(message);
	}

	@Message(encoders = UserInfoMessageSerializer.class, decoders = UserInfoMessageSerializer.class)
	public AbstractWebsocketMessage onUserInfoMessage(UserInfoMessage message) throws IOException {
		return getHandler().onUserInfoMessage(message);
	}

	@Message(encoders = HistoMessageSerializer.class, decoders = HistoMessageSerializer.class)
	public AbstractWebsocketMessage onHistoMessage(HistoMessage message) throws IOException {
		return getHandler().onHistoMessage(message);
	}

	@Message(encoders = FileSharingMessageSerializer.class, decoders = FileSharingMessageSerializer.class)
	public AbstractWebsocketMessage onFileSharingMessage(FileSharingMessage message) {
		return getHandler().onFileSharingMessage(message);
	}

	private WebsocketMessageHandler getHandler() {
		if (roomName == null || userName == null) {
			throw new RuntimeException("Something is definitely not okay. ROOM: " + roomName + " - USER: " + userName);
		}
		return websocketMessageHandlerRegistry.getHandler(roomName, userName);
	}
}

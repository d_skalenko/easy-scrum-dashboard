package dashboard.websockets.service.filesharing;

import java.io.IOException;

import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import dashboard.websockets.serializer.StringSerializer;

@ManagedService(path = "/ws/filesharing", interceptors = {
		AtmosphereResourceLifecycleInterceptor.class
})
public class FilesharingWebsocketsService extends SpringBeanAutowiringSupport {

	@Ready
	public void onReady(final AtmosphereResource resource) {
	}

	@Disconnect
	public void onDisconnect(AtmosphereResourceEvent event) {
	}

	@Message(encoders = StringSerializer.class, decoders = StringSerializer.class)
	public String onMessage(String message) throws IOException {
		return message;
	}
}

package dashboard.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import dashboard.persistance.service.PersistenceService;
import dashboard.persistance.service.UserPicService;

/**
 * Created by vx on 8/16/2015.
 */
@Controller
public class UserPicController {

	public static final String UPLOAD_URL = "/userPic/upload";
	public static final String DOWNLOAD_URL = "/userPic/download/";
	@Autowired
	private UserPicService userPicService;

	@Autowired
	private PersistenceService persistenceService;

	@POST
	@RequestMapping(value = UPLOAD_URL, method = RequestMethod.POST)
	public
	@ResponseBody
	Response uploadFile(@RequestParam("userName") String userName, @RequestParam("file_data") MultipartFile file) {
		byte[] bytes;
		try {
			String extension = getExtension(file);
			String fileName = userName + "-avatar-" + System.currentTimeMillis() + "." + extension;

			bytes = file.getBytes();
			userPicService.persist(fileName, bytes);
			String old = persistenceService.changeAvatar(userName, DOWNLOAD_URL + fileName);
			if (old != null) {
				try {
					userPicService.remove(old.replace(DOWNLOAD_URL, ""));
				} catch (Exception e) {}
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		return Response.status(Response.Status.OK).build();
	}

	public String getExtension(MultipartFile file) {
		String[] split = file.getOriginalFilename().split("[.]");
		String extension = split[split.length - 1].toLowerCase();
		userPicService.checkExtensionAllowed(extension);
		return extension;
	}

	/**
	 * {fileName:.+} => prevents DOT truncation
	 * @see <a href="http://stackoverflow.com/questions/16332092/spring-mvc-pathvariable-with-dot-is-getting-truncated">Stack overflow issue</a>
	 */
	@GET
	@RequestMapping(value = DOWNLOAD_URL + "{fileName:.+}", method = RequestMethod.GET)
	public
	@ResponseBody
	byte[] downloadFile(@PathVariable("fileName") String fileName) {
		return userPicService.get(fileName).getContent();
	}
}

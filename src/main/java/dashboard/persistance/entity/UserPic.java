package dashboard.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Created by vx on 8/16/2015.
 */
@Entity
@Table(name = "userPics")
public class UserPic {

	@Id
	private String fileName;

	@Lob
	@Column(nullable = false)
	private byte[] content;

	public UserPic() {
	}

	public UserPic(String fileName, byte[] content) {
		this.fileName = fileName;
		this.content = content;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}
}

package dashboard.persistance.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Created by vx on 8/2/2015.
 */
@Entity
@Table(name = "tweets")
public class StatusTweet {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@Length(max = 140)
	private String text;

	private String sender;

	private String team;

	@NotNull
	private Date date;

	@PrePersist
	void createdAt() {
		this.date = new Date();
	}

	public long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, text, sender, date);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final StatusTweet other = (StatusTweet) obj;
		return Objects.equals(this.id, other.id) && Objects.equals(this.text, other.text) && Objects.equals(this.sender, other.sender) && Objects
				.equals(this.date, other.date);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("StatusTweet{");
		sb.append("id=").append(id);
		sb.append(", text='").append(text).append('\'');
		sb.append(", sender=").append(sender);
		sb.append(", team=").append(team);
		sb.append(", date=").append(date);
		sb.append('}');
		return sb.toString();
	}
}

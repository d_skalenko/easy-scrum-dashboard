package dashboard.persistance.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by vx on 8/23/2015.
 */
@Entity
@Table(name = "chatlogs")
public class ChatHistoMessage {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	private String text;

	@NotNull
	private String sender;

	@NotNull
	private String receiver;

	@NotNull
	private Date date;

	public ChatHistoMessage() {
	}

	public ChatHistoMessage(String text, String sender, String receiver, Date date) {
		this.text = text;
		this.sender = sender;
		this.receiver = receiver;
		this.date = date;
	}

	@PrePersist
	void createdAt() {
		this.date = new Date();
	}

	public long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, text, sender, receiver, date);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final ChatHistoMessage other = (ChatHistoMessage) obj;
		return Objects.equals(this.id, other.id) && Objects.equals(this.text, other.text) && Objects.equals(this.sender, other.sender) && Objects
				.equals(this.receiver, other.receiver) && Objects.equals(this.date, other.date);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ChatMessage{");
		sb.append("id=").append(id);
		sb.append(", text='").append(text).append('\'');
		sb.append(", sender='").append(sender).append('\'');
		sb.append(", receiver='").append(receiver).append('\'');
		sb.append(", date=").append(date);
		sb.append('}');
		return sb.toString();
	}
}

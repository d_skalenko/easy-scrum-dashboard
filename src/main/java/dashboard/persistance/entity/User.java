package dashboard.persistance.entity;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

/**
 * @author AChernyak
 * @date July 03, 2015
 * @copyright Swissquote Bank SA
 */
@Entity
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@Column(unique = true)
	private String name;

	@NotNull
	private String role;

	private String userPic;

	@NotNull
	private boolean online = false;

	@ManyToMany(targetEntity = TeamInfo.class, mappedBy = "members", fetch = FetchType.EAGER)
	private Set<TeamInfo> teams;

	@OneToMany(targetEntity = StatusTweet.class, fetch = FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.EXTRA)
	@OrderBy("id DESC")
	private List<StatusTweet> tweets;

	public User() {
	}

	public User(String name, String role) {
		this.name = name;
		this.role = role;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserPic() {
		return userPic;
	}

	public void setUserPic(String userPic) {
		this.userPic = userPic;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public Set<TeamInfo> getTeams() {
		return teams;
	}

	public void setTeams(Set<TeamInfo> teams) {
		this.teams = teams;
	}

	public List<StatusTweet> getTweets() {
		return tweets;
	}

	public void setTweets(List<StatusTweet> tweets) {
		this.tweets = tweets;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final User other = (User) obj;
		return Objects.equals(this.id, other.id) && Objects.equals(this.name, other.name);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("User{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", role='").append(role).append('\'');
		sb.append(", userPic='").append(userPic).append('\'');
		sb.append(", online=").append(online);
		sb.append(", teams=").append(Collections2.transform(teams, new Function<TeamInfo, String>() {
			@Override
			public String apply(TeamInfo input) {
				return input.getName();
			}
		}));
		sb.append(", tweets=").append(tweets);
		sb.append('}');
		return sb.toString();
	}
}

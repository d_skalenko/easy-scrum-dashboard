package dashboard.persistance.entity;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Created by vx on 8/1/2015.
 */
@Entity
@Table(name = "teams")
public class TeamInfo {

	@Id
	@NotNull
	private String name;

	@ManyToMany(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)

	@JoinColumn(name = "userId")
	private Set<User> members;

	/**
	 * sorted by date list of tweets (higher ID = later)
	 */
	@ManyToMany(targetEntity = StatusTweet.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.EXTRA)
	@OrderBy("id DESC")
	private List<StatusTweet> tweets;

	public TeamInfo() {
	}

	public TeamInfo(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getMembers() {
		return members;
	}

	public void setMembers(Set<User> members) {
		this.members = members;
	}

	public List<StatusTweet> getTweets() {
		return tweets;
	}

	public void setTweets(List<StatusTweet> tweets) {
		this.tweets = tweets;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final TeamInfo other = (TeamInfo) obj;
		return Objects.equals(this.name, other.name);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("TeamInfo{");
		sb.append(", name='").append(name).append('\'');
		sb.append(", members=").append(members);
		sb.append(", tweets=").append(tweets);
		sb.append('}');
		return sb.toString();
	}
}

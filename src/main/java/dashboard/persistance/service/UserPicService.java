package dashboard.persistance.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dashboard.persistance.dao.UserPicDao;
import dashboard.persistance.entity.UserPic;

/**
 * Created by vx on 8/17/2015.
 */
@Service
@Transactional
public class UserPicService {

	@Autowired
	private UserPicDao userPicDao;

	public static final List<String> ALLOWED_EXTENSIONS = Arrays.asList("jpg","jpeg","png","gif", "bmp");

	public void persist(String fileName, byte[] data) {
		userPicDao.save(new UserPic(fileName, data));
	}

	public UserPic get(String fileName) {
		return userPicDao.findOne(fileName);
	}

	public void remove(String fileName) {
		userPicDao.delete(fileName);
	}

	public void checkExtensionAllowed(String extension) {
		if (!ALLOWED_EXTENSIONS.contains(extension)) {
			throw new IllegalArgumentException(extension + " is not an allowed extension. Only " + ALLOWED_EXTENSIONS + " are supported");
		}
	}
}

package dashboard.persistance.service;

import dashboard.persistance.dao.ChatMessageDao;
import dashboard.persistance.entity.ChatHistoMessage;
import dashboard.websockets.message.AbstractWebsocketMessage;
import dashboard.websockets.message.ChatMessage;
import dashboard.websockets.message.MessageType;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;

/**
 * Created by vx on 8/24/2015.
 */
@Service
@Transactional
public class ChatLogsService {

	@Autowired
	private ChatMessageDao chatMessageDao;

	public void persistMessage(ChatMessage message) {
		chatMessageDao.save(new ChatHistoMessage(message.getMessage(), message.getSender(), message.getReceiver(), new Date(message.getTime())));
	}

	public List<AbstractWebsocketMessage> getRecentLogs(String team) {
		return FluentIterable.from(chatMessageDao.findByReceiver(team, new PageRequest(0, 100, new Sort(new Sort.Order(Sort.Direction.DESC, "id")))))
				.transform(new Function<ChatHistoMessage, ChatMessage>() {
					@Override
					public ChatMessage apply(ChatHistoMessage input) {
						ChatMessage chatMessage = new ChatMessage(input.getText(), input.getSender(), MessageType.CHAT);
						chatMessage.setReceiver(input.getReceiver());
						chatMessage.setTime(input.getDate().getTime());
						return chatMessage;
					}
				})
				.toList().reverse();
	}
}

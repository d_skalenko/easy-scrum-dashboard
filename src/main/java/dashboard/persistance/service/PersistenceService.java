package dashboard.persistance.service;

import dashboard.persistance.dao.TeamDao;
import dashboard.persistance.dao.UserDao;
import dashboard.persistance.entity.StatusTweet;
import dashboard.persistance.entity.TeamInfo;
import dashboard.persistance.entity.User;
import dashboard.websockets.message.LoginMessage;
import dashboard.websockets.message.TweetMessage;
import dashboard.websockets.message.UserProfileRequestMessage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * Created by vx on 8/15/2015.
 */
@Service
@Transactional
public class PersistenceService implements InitializingBean {

	@Autowired
	private TeamDao teamDao;

	@Autowired
	private UserDao userDao;

	public TeamInfo getOrCreateTeam(String teamName) {
		TeamInfo teamInfo = teamDao.findOne(teamName);
		if (teamInfo == null) {
			teamInfo = new TeamInfo(teamName);
			teamInfo.setMembers(new HashSet<User>());
			teamInfo.setTweets(new ArrayList<StatusTweet>());
			teamDao.save(teamInfo);
		}
		return teamInfo;
	}

	public TeamInfo logInUser(LoginMessage loginMessage) {
		TeamInfo teamInfo = teamDao.findOne(loginMessage.getTeam());
		User user = userDao.findByName(loginMessage.getSender());
		if (user == null) {
			// save user to db
			user = new User(loginMessage.getSender(), "user");
			HashSet<TeamInfo> teamInfos = new HashSet<TeamInfo>();
			teamInfos.add(teamInfo);
			user.setTeams(teamInfos);
			user.setTweets(new ArrayList<StatusTweet>());
			userDao.save(user);
		}
		user.setOnline(true);

		if (!teamInfo.getMembers().contains(user)) {
			teamInfo.getMembers().add(user);
		}
		return teamInfo;
	}

	public void logOutUser(String userName) {
		User user = userDao.findByName(userName);
		if (user != null) {
			user.setOnline(false);
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Iterable<User> all = userDao.findAll();
		for (User user : all) {
			user.setOnline(false);
			userDao.save(user);
		}
	}

	public void persistTweet(TweetMessage tweetMessage) {
		TeamInfo teamInfo = teamDao.findOne(tweetMessage.getTeam());
		StatusTweet statusTweet = new StatusTweet();
		User user = userDao.findByName(tweetMessage.getSender());
		statusTweet.setSender(user.getName());
		statusTweet.setTeam(tweetMessage.getTeam());
		statusTweet.setText(tweetMessage.getText());

		List<StatusTweet> tweets = user.getTweets();
		if (tweets != null) {
			tweets.add(statusTweet);
		} else {
			tweets = Lists.newArrayList(statusTweet);
			user.setTweets(tweets);
		}
		teamInfo.getTweets().add(statusTweet);
	}

	/**
	 * @return old avatar
	 */
	public String changeAvatar(String userName, String avatarPath) {
		User user = userDao.findByName(userName);
		String oldUserPic = null;
		if (user != null) {
			oldUserPic = user.getUserPic();
			user.setUserPic(avatarPath);
			userDao.save(user);
		}
		return oldUserPic;
	}

	public User getUserProfile(UserProfileRequestMessage userProfileRequestMessage) {
		User user = userDao.findByName(userProfileRequestMessage.getUserName());
		Hibernate.initialize(user.getTweets());
		return user;
	}
}

package dashboard.persistance.dao;

import org.springframework.data.repository.CrudRepository;

import dashboard.persistance.entity.User;

/**
 * Created by vx on 8/1/2015.
 */
public interface UserDao extends CrudRepository<User, Long> {

	User findByName(String name);

}

package dashboard.persistance.dao;

import dashboard.persistance.entity.ChatHistoMessage;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by vx on 8/24/2015.
 */
public interface ChatMessageDao extends CrudRepository<ChatHistoMessage, Long>, PagingAndSortingRepository<ChatHistoMessage, Long> {

	public Page findByReceiver(String receiver, Pageable pageable);

}

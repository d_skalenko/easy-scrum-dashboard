package dashboard.persistance.dao;

import dashboard.persistance.entity.UserPic;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by vx on 8/17/2015.
 */
public interface UserPicDao extends CrudRepository<UserPic, String> {
}

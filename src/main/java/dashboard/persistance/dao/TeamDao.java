package dashboard.persistance.dao;

import dashboard.persistance.entity.TeamInfo;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by vx on 8/2/2015.
 */
public interface TeamDao extends CrudRepository<TeamInfo, String> {

}

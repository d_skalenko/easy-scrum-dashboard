package dashboard.persistance.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import dashboard.persistance.entity.StatusTweet;

public interface TweetDao extends CrudRepository<StatusTweet, Long>, PagingAndSortingRepository<StatusTweet, Long> {

	public Page findAll(Pageable pageable);

	public Page<StatusTweet> findByTeam(String team, Pageable pageable);
}

package dashboard.service;

import org.atmosphere.cpr.Broadcaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dashboard.domain.Room;
import dashboard.domain.RoomMember;
import dashboard.websockets.message.AbstractWebsocketMessage;
import dashboard.websockets.message.ChatMessage;
import dashboard.websockets.message.Direction;
import dashboard.websockets.message.FileSharingMessage;
import dashboard.websockets.message.HistoMessage;
import dashboard.websockets.message.LoginMessage;
import dashboard.websockets.message.MessageType;
import dashboard.websockets.message.TeamInfoMessage;
import dashboard.websockets.message.TweetMessage;
import dashboard.websockets.message.UserInfoMessage;
import dashboard.websockets.message.UserProfileRequestMessage;

/**
 * Created by vx on 2/7/2016.
 */
public class WebsocketMessageHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebsocketMessageHandler.class);

	private final String roomName;
	private final String userName;
	private final RoomRegistry roomRegistry;

	public WebsocketMessageHandler(String roomName, String userName, RoomRegistry roomRegistry) {
		this.roomName = roomName;
		this.userName = userName;
		this.roomRegistry = roomRegistry;
	}

	public void onConnect(Broadcaster broadcaster) {
		Room room = getRoom();
		RoomMember member = new RoomMember(roomName, userName, broadcaster);
		room.registerBroadcaster(member);
		room.broadcastRoomStatus(member);
	}

	public void onDisconnect() {
		getRoom().logOut(userName, roomName);
	}

	public AbstractWebsocketMessage onChatMessage(ChatMessage message) {
		if (MessageType.CHAT.equals(message.getType()) && Direction.TO_SERVER.equals(message.getDirection())) {
			LOGGER.info("Author {} sent message {}", message.getSender(),
					message.getMessage());
			message.setReceiver(roomName);
			getRoom().propagateAndPersistChatMessage(message);
			return null;
		}
		return message;
	}

	public AbstractWebsocketMessage onLogin(LoginMessage message) {
		getRoom().logIn(message);
		return message;
	}

	public AbstractWebsocketMessage onTeamInfoRequest(TeamInfoMessage message) {
		return message;
	}

	public AbstractWebsocketMessage onTweet(TweetMessage message) {
		if (message.getText() != null && Direction.TO_SERVER.equals(message.getDirection())) {
			LOGGER.info("Author {}({}) sent tweet {}", message.getSender(), message.getTeam(),
					message.getText());
			getRoom().propagateAndPersistTweet(message);
			return null;
		}
		return message;
	}

	public AbstractWebsocketMessage onUserProfileRequest(UserProfileRequestMessage message) {
		getRoom().broadcastUserData(message);
		return null;
	}

	public AbstractWebsocketMessage onUserInfoMessage(UserInfoMessage message) {
		return message;
	}

	public AbstractWebsocketMessage onHistoMessage(HistoMessage message) {
		return message;
	}

	public AbstractWebsocketMessage onFileSharingMessage(FileSharingMessage message) {
		if (Direction.TO_SERVER.equals(message.getDirection())) {
			getRoom().privateBroadcast(message.getToUser(), message);
			return null;
		}
		return message;
	}

	private Room getRoom() {
		return roomRegistry.get(roomName);
	}
}

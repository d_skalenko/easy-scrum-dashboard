package dashboard.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import akka.actor.ActorSystem;
import dashboard.domain.Room;

/**
 * Abstract room manager that binds akka and atmosphere pub/sub
 */
@Service
public class RoomRegistry {

	private final Map<String, Room> rooms = new ConcurrentHashMap<String, Room>();

	@Autowired
	private ActorSystem actorSystem;

	public Room get(String roomName) {
		Room room = rooms.get(roomName);
		if (room == null) {
			room = new Room(roomName, actorSystem);
			rooms.put(roomName, room);
		}
		return room;
	}

}

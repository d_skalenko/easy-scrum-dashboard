package dashboard.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

/**
 * Created by vx on 2/8/2016.
 */
@Component
public class WebsocketMessageHandlerRegistry {

	@Autowired
	private RoomRegistry roomRegistry;

	private Table<String, String, WebsocketMessageHandler> registry = HashBasedTable.create();

	public WebsocketMessageHandler getHandler(String roomName, String userName) {
		WebsocketMessageHandler websocketMessageHandler = registry.get(roomName, userName);
		if (websocketMessageHandler == null) {
			websocketMessageHandler = new WebsocketMessageHandler(roomName, userName, roomRegistry);
			registry.put(roomName, userName, websocketMessageHandler);
		}
		return websocketMessageHandler;
	}

	public Map<String, WebsocketMessageHandler> getUserHandlersByRoom(String userName) {
		return registry.column(userName);
	}
}

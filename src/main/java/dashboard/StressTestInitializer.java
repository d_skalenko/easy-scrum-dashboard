package dashboard;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import dashboard.persistance.dao.ChatMessageDao;
import dashboard.persistance.dao.TeamDao;
import dashboard.persistance.entity.ChatHistoMessage;
import dashboard.persistance.entity.StatusTweet;
import dashboard.persistance.entity.TeamInfo;
import dashboard.persistance.entity.User;

@Component
@Transactional
public class StressTestInitializer implements InitializingBean {

	public static final int TWEETS_TO_GENERATE = 10000;
	@Autowired
	private TeamDao teamDao;

	@Autowired
	private ChatMessageDao chatMessageDao;

	@Override
	public void afterPropertiesSet() throws Exception {
		TeamInfo fromDb = teamDao.findOne("test");
		if (fromDb != null) {
			return;
		}
		TeamInfo test = new TeamInfo("test");
		User user1 = new User("user1", "user");
		User user2 = new User("user2", "user");

		test.setMembers(Sets.newHashSet(user1, user2));

		List<StatusTweet> tweets = Lists.newArrayListWithExpectedSize(TWEETS_TO_GENERATE);
		List<ChatHistoMessage> chat = Lists.newArrayListWithExpectedSize(TWEETS_TO_GENERATE);

		for	(int i = 0; i < TWEETS_TO_GENERATE; i++) {
			StatusTweet tw = new StatusTweet();
			tw.setSender(user1.getName());
			tw.setTeam(test.getName());
			tw.setText("test message <" + i + ">");
			tweets.add(tw);
			// also chat messages
			chat.add(new ChatHistoMessage("chat" + i, user1.getName(), test.getName(), new Date()));
		}
		chatMessageDao.save(chat);

		test.setTweets(tweets);

		teamDao.save(test);
		System.out.println("!!!!!!!!!!!!! Test dataset for team <test> saved");
	}
}
